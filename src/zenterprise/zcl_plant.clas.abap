CLASS zcl_plant DEFINITION
  PUBLIC
  CREATE PUBLIC .

  PUBLIC SECTION.
    TYPES : BEGIN OF ts_key,
              plant TYPE t001w-werks,
            END OF ts_key.
    METHODS constructor IMPORTING is_key TYPE ts_key.
    METHODS get_address RETURNING VALUE(ro_address) TYPE ref to zcl_address
                        RAISING   zcx_object_not_found.
  PROTECTED SECTION.
  PRIVATE SECTION.
    DATA : ms_key TYPE ts_key.
    DATA : BEGIN OF ms_data,
             adrnr TYPE t001w-adrnr,
           END OF ms_data.
    DATA : mo_address TYPE ref to zcl_address.
    METHODS read_data RAISING zcx_object_not_found.

ENDCLASS.



CLASS ZCL_PLANT IMPLEMENTATION.


  METHOD constructor.
*-----------------------------------------------------------------------*
* Description :                                                         *
*   Constructor                                                         *
*-----------------------------------------------------------------------*
* CHANGE HISTORY                                                        *
* Date       Dev Ref#         Author                                    *
* ========== ==========       ========================================= *
*            ABAP BASE BUILD  Wilbert SIson                             *
* Description : Initial Development                                     *
*-----------------------------------------------------------------------*

    ms_key = is_key.

  ENDMETHOD.


  METHOD get_address.
*-----------------------------------------------------------------------*
* Description :                                                         *
*   Get Plant Address Object                                            *
*-----------------------------------------------------------------------*
* CHANGE HISTORY                                                        *
* Date       Dev Ref#         Author                                    *
* ========== ==========       ========================================= *
*            ABAP BASE BUILD  Wilbert SIson                             *
* Description : Initial Development                                     *
*-----------------------------------------------------------------------*

    IF mo_address IS INITIAL.
      read_data( ).
    ENDIF.

    mo_address = NEW zcl_address( conv #( ms_data-adrnr ) ).
    ro_address = mo_address.

  ENDMETHOD.


  METHOD read_data.
*-----------------------------------------------------------------------*
* Description :                                                         *
*   Read context data for the Plant                                     *
*-----------------------------------------------------------------------*
* CHANGE HISTORY                                                        *
* Date       Dev Ref#         Author                                    *
* ========== ==========       ========================================= *
*            ABAP BASE BUILD  Wilbert SIson                             *
* Description : Initial Development                                     *
*-----------------------------------------------------------------------*

    CHECK ms_data IS INITIAL.
    SELECT SINGLE adrnr FROM t001w
      INTO ms_data
      WHERE werks = ms_key.
    IF sy-subrc NE 0.
      MESSAGE e001(zenterprise) WITH ms_key INTO DATA(message_dummy).
      RAISE EXCEPTION TYPE zcx_object_not_found
        EXPORTING
          messages = NEW zcl_messages( )->add_system_message( )->mt_messages.
    ENDIF.

  ENDMETHOD.
ENDCLASS.
