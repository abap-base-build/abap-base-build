
CLASS zcl_address DEFINITION
  PUBLIC
  CREATE PUBLIC .

  PUBLIC SECTION.
    TYPES : BEGIN OF ts_key,
              address TYPE t001w-adrnr,
            END OF ts_key.
    METHODS constructor IMPORTING is_key TYPE ts_key.
    METHODS get_data RETURNING VALUE(rs_data) TYPE sadr
                     RAISING   zcx_object_not_found.
  PROTECTED SECTION.
  PRIVATE SECTION.
    DATA : ms_key TYPE ts_key.
    DATA : ms_data TYPE sadr.
    METHODS read_data RAISING zcx_object_not_found.

ENDCLASS.



CLASS ZCL_ADDRESS IMPLEMENTATION.


  METHOD constructor.
*-----------------------------------------------------------------------*
* Description :                                                         *
*   Constructor                                                         *
*-----------------------------------------------------------------------*
* CHANGE HISTORY                                                        *
* Date       Dev Ref#         Author                                    *
* ========== ==========       ========================================= *
*            ABAP BASE BUILD  Wilbert SIson                             *
* Description : Initial Development                                     *
*-----------------------------------------------------------------------*

    ms_key = is_key.
  ENDMETHOD.


  METHOD get_data.
*-----------------------------------------------------------------------*
* Description :                                                         *
*   Return address data                                                 *
*-----------------------------------------------------------------------*
* CHANGE HISTORY                                                        *
* Date       Dev Ref#         Author                                    *
* ========== ==========       ========================================= *
*            ABAP BASE BUILD  Wilbert SIson                             *
* Description : Initial Development                                     *
*-----------------------------------------------------------------------*

    read_data( ).

    rs_data = ms_data.

  ENDMETHOD.


  METHOD read_data.
*-----------------------------------------------------------------------*
* Description :                                                         *
*   Read address data                                                   *
*-----------------------------------------------------------------------*
* CHANGE HISTORY                                                        *
* Date       Dev Ref#         Author                                    *
* ========== ==========       ========================================= *
*            ABAP BASE BUILD  Wilbert SIson                             *
* Description : Initial Development                                     *
*-----------------------------------------------------------------------*

    CHECK ms_data IS INITIAL.
    CALL FUNCTION 'ADDR_GET'
      EXPORTING
        address_selection = VALUE addr1_sel( addrnumber = ms_key )
      IMPORTING
        sadr              = ms_data
      EXCEPTIONS
        OTHERS            = 4.
    IF sy-subrc NE 0.
      MESSAGE e001(zcrossapp) WITH ms_key INTO DATA(message_dummy).
      RAISE EXCEPTION TYPE zcx_object_not_found
        EXPORTING
          messages = NEW zcl_messages( )->add_system_message( )->mt_messages.
    ENDIF.

  ENDMETHOD.
ENDCLASS.
