class ZCX_BAPI_MESSAGES definition
  public
  inheriting from CX_STATIC_CHECK
  create public .

public section.

  data MESSAGES type BAPIRET2_T .

  methods CONSTRUCTOR
    importing
      !TEXTID like TEXTID optional
      !PREVIOUS like PREVIOUS optional
      !MESSAGES type BAPIRET2_T optional .
protected section.
private section.
ENDCLASS.



CLASS ZCX_BAPI_MESSAGES IMPLEMENTATION.


  method CONSTRUCTOR.
CALL METHOD SUPER->CONSTRUCTOR
EXPORTING
TEXTID = TEXTID
PREVIOUS = PREVIOUS
.
me->MESSAGES = MESSAGES .
  endmethod.
ENDCLASS.
