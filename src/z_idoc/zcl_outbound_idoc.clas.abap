CLASS zcl_outbound_idoc DEFINITION
  PUBLIC
  CREATE PUBLIC .

  PUBLIC SECTION.

    METHODS constructor
      IMPORTING
        !io_mapper TYPE REF TO zif_outbound_idoc_mapper .
    METHODS send
      RETURNING
        VALUE(rt_messages) TYPE bapiret2_t
      RAISING
        zcx_rhl_invoice_idoc .
    CLASS-METHODS
      map_message_to_idoc_status
        IMPORTING is_message            TYPE bapiret2
        RETURNING VALUE(rs_idoc_status) TYPE bdidocstat.
  PROTECTED SECTION.

    DATA mo_mapper TYPE REF TO zif_outbound_idoc_mapper .

    METHODS get_control_record
      RETURNING
        VALUE(rs_edi_control) TYPE edidc .
  PRIVATE SECTION.
ENDCLASS.



CLASS ZCL_OUTBOUND_IDOC IMPLEMENTATION.


  METHOD constructor.
*-----------------------------------------------------------------------*
* Description :                                                         *
*   Constructor - store the mapper instance                             *
*-----------------------------------------------------------------------*
* CHANGE HISTORY                                                        *
* Date       Dev Ref#         Author                                    *
* ========== ==========       ========================================= *
*            ABAP BASE BUILD  Wilbert SIson                             *
* Description : Initial Development                                     *
*-----------------------------------------------------------------------*

    mo_mapper ?= io_mapper.

  ENDMETHOD.


  METHOD get_control_record.
*-----------------------------------------------------------------------*
* Description :                                                         *
*   Initialize the idoc control record                                  *
*-----------------------------------------------------------------------*
* CHANGE HISTORY                                                        *
* Date       Dev Ref#         Author                                    *
* ========== ==========       ========================================= *
*            ABAP BASE BUILD  Wilbert SIson                             *
* Description : Initial Development                                     *
*-----------------------------------------------------------------------*

*    Redefine at subclass

  ENDMETHOD.


  METHOD map_message_to_idoc_status.
*-----------------------------------------------------------------------*
* Description :                                                         *
*   Map the message fields to idoc status fields                        *
*-----------------------------------------------------------------------*
* CHANGE HISTORY                                                        *
* Date       Dev Ref#         Author                                    *
* ========== ==========       ========================================= *
*            ABAP BASE BUILD  Wilbert SIson                             *
* Description : Initial Development                                     *
*-----------------------------------------------------------------------*

    CLEAR rs_idoc_status.
    rs_idoc_status-msgid  = is_message-id.
    rs_idoc_status-msgty  = is_message-type.
    rs_idoc_status-msgno  = is_message-number.
    rs_idoc_status-msgv1  = is_message-message_v1.
    rs_idoc_status-msgv2  = is_message-message_v2.
    rs_idoc_status-msgv3  = is_message-message_v3.
    rs_idoc_status-msgv4  = is_message-message_v4.

  ENDMETHOD.


  METHOD send.
*-----------------------------------------------------------------------*
* Description :                                                         *
*   Send the RHL Invoice Idoc                                           *
*-----------------------------------------------------------------------*
* CHANGE HISTORY                                                        *
* Date       Dev Ref#     Author                                        *
* ========== ==========   ============================================= *
* 10.10.2020 E025         Wilbert Sison                                 *
* Description : Initial Development                                     *
*-----------------------------------------------------------------------*

    DATA(ls_control_record) = get_control_record( ).
    DATA(lt_control_record) = VALUE edidc_tt( ( ls_control_record ) ).
    DATA(lt_idoc_data)      = mo_mapper->get_mapped_data( ).
    CALL FUNCTION 'MASTER_IDOC_DISTRIBUTE'
      EXPORTING
        master_idoc_control            = ls_control_record
      TABLES
        communication_idoc_control     = lt_control_record
        master_idoc_data               = lt_idoc_data
      EXCEPTIONS
        error_in_idoc_control          = 1
        error_writing_idoc_status      = 2
        error_in_idoc_data             = 3
        sending_logical_system_unknown = 4
        OTHERS                         = 5.
    IF sy-subrc NE 0.
      RAISE EXCEPTION TYPE zcx_rhl_invoice_idoc
        EXPORTING
          messages = NEW zcl_messages( )->add_system_message( )->mt_messages.
    ENDIF.

    LOOP AT lt_control_record ASSIGNING FIELD-SYMBOL(<ls_control>)
      WHERE status NE space.
      EXIT.
    ENDLOOP.
    IF sy-subrc EQ 0.
      MESSAGE i008(zz01) WITH <ls_control>-docnum INTO DATA(message_dummy).
      rt_messages = NEW zcl_messages( )->add_system_message( )->mt_messages.
    ENDIF.

    CALL FUNCTION 'DB_COMMIT'. "SAP Note 150202
    CALL FUNCTION 'DEQUEUE_ALL'
      EXPORTING
        _synchron = 'X'.
    COMMIT WORK.
  ENDMETHOD.
ENDCLASS.
