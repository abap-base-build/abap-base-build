# README #


### What is this repository for? ###

ABAP Base Build is a collection of classes that is intended to facilitate Rapid Development.  I mainly use it to reduce my setup work for components and provides a common simplified syntax for several technical areas

* Enterprise Services
* Idoc Management
* Report Development
* BRF
* Adobe 

### How do I get set up? ###

* Keep posted in this area.


### Contribution guidelines ###

* Please contact Repo owner.

### Who do I talk to? ###

 
Wilbert Sison   
Principal Consultant  
**DT SOLUTIONS**  
wcsison@gmail.com